import css from './index.scss';

$(document).ready(function () {
  'use strict';

  var interval_id;
  var progress_interval_id;
  var properties = {
    width: 1920,         // Specify the width in pixels of the pattern to generate.
    height: 1080,        // Specify the height in pixels of the pattern to generate.
    cell_size: 50,       // Specify the size of the mesh used to generate triangles.
    variance: 1,         // Specify the amount of randomness used when generating triangles.

    color_space: 'lab',  // Set the color space used for generating gradients
    stroke_width: 1.51,  // Specify the width of the stroke on triangle shapes in the pattern
    seed: null,          // Seeds the random number generator to create repeatable patterns.

    color_function: false // WON'T be implemented
                          // Specify a custom function for coloring triangles
    /*
    x_colors: 'random',
    y_colors: 'match_x',
    palette: Trianglify.colorbrewer,
    */
  };

  function generate(format = 'html') {
    var pattern = Trianglify(properties);

    if (format == 'html') {
      $('#viewport .render-container').html('<canvas id="preview"></canvas>');
      pattern.canvas(document.getElementById('preview'));
    } else if (format == 'svg') {
      $('#viewport .render-container').html(pattern.svg({includeNamespace: true}));
    } else if (format == 'png') {
      $('#viewport .render-container').html('<img id="preview" src="" />');
      $('#viewport #preview').attr('src', pattern.png());
    } else {
      console.log('unknown format.');
    }
  }

  function setup() {
    $('#width').attr('value', properties.width);
    $('#height').attr('value', properties.height);
    $('#variance').attr('value', properties.variance);
    $('#cell_size').attr('value', properties.cell_size);
    $('#color_space').val(properties.color_space);
    $('#stroke_width').attr('value', properties.stroke_width);
    $('#seed').attr('value', properties.seed);

    function metadata() {
      clearInterval(progress_interval_id);
      progress_interval_id = setInterval(function() {
        var width = $('#viewport .progress-bar').attr('aria-valuenow') - .100;
        $('#viewport .progress-bar').attr('aria-valuenow', width);
        $('#viewport .progress-bar').attr('style', 'width: ' + width * 10 + '%');
      }, 100);

      $('#viewport .metadata').html(
        '<div class="progress" style="height: 0.75em;">' +
        '  <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar"' +
        '    style="width: 100%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="10">' +
        '  </div>' +
        '</div>'
      );
    }

    interval_id = setInterval(function() {
      metadata();
      generate();
    }, 10000);

    metadata();
    generate();
  }

  setup();

  $('#width').on('change', function() {
    properties.width = parseInt($(this).val(), 10);
    generate();
  });
  $('#height').on('change', function() {
    properties.height = parseInt($(this).val(), 10);
    generate();
  });
  $('#variance').on('change', function() {
    properties.variance = parseInt($(this).val(), 10);
    generate();
  });
  $('#cell_size').on('change', function() {
    properties.cell_size = parseInt($(this).val(), 10);
    generate();
  });
  $('#color_space').on('change', function() {
    properties.color_space = $(this).val();
    generate();
  });
  $('#stroke_width').on('change', function() {
    properties.stroke_width = parseInt($(this).val(), 10);
    generate();
  });
  $('#seed').on('change', function() {
    properties.seed = $(this).val() == '' ? null : $(this).val() ;
    generate();
  });


  $('#seed_clear').on('click', function() {
    $('#seed').attr('value', '');
    $('#seed').val('');
    $('#seed').trigger('change');
  });
  $('#seed_refresh').on('click', function() {
    var crypto = require('crypto');
    var seed = crypto.randomBytes(4).toString('hex');

    $('#seed').attr('value', seed);
    $('#seed').val(seed);
    $('#seed').trigger('change');
  });

  $('#svg').on('click', function() {
    generate('svg');
    $('#controls').trigger('change');
  });
  $('#png').on('click', function() {
    generate('png');
    $('#controls').trigger('change');
  });
  $('#html').on('click', function() {
    generate('html');
    $('#controls').trigger('change');
  });

  $('#controls').on('change', function() {
    clearInterval(interval_id);
    clearInterval(progress_interval_id);
    $('#viewport .metadata').html('');
  });
});
